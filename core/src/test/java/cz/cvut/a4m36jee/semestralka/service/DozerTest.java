package cz.cvut.a4m36jee.semestralka.service;

import cz.cvut.a4m36jee.semestralka.model.dto.UserDto;
import cz.cvut.a4m36jee.semestralka.model.entity.Role;
import cz.cvut.a4m36jee.semestralka.model.entity.User;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.util.Collections;

import static junit.framework.Assert.*;

/**
 * @author Tomas Milata
 * @since 2.2.15.
 */
@RunWith(Arquillian.class)
public class DozerTest {

    @Inject
    private Service service;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Service.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }


    @Test
    public void mapUserToDto() {
        final User user = new User();
        user.setId(1);
        user.setEmail("email");

        final Role role = new Role();
        role.setName(cz.cvut.a4m36jee.semestralka.security.Role.ADMIN);
        user.setRoles(Collections.singleton(role));


        UserDto dto = service.objectMapper.map(user, UserDto.class);

        assertNotNull(dto);
        assertEquals(dto.getId(), user.getId());
        assertEquals(dto.getEmail(), user.getEmail());

        assertTrue(dto.getRolesNames().contains(cz.cvut.a4m36jee.semestralka.security.Role.ADMIN));
    }

    @Dependent
    public static class Service extends AbstractService {


    }
}
