package cz.cvut.a4m36jee.semestralka.batch;

import cz.cvut.a4m36jee.semestralka.model.entity.JpaEntity;
import cz.cvut.a4m36jee.semestralka.model.entity.Post;
import junit.framework.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * @author Tomas Milata
 * @since 31.1.15.
 */
@RunWith(Arquillian.class)
public class DirtyWordsBatchWriterTest {

    @Inject
    private DirtyWordsBatchProcessor processor;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(DirtyWordsBatchProcessor.class)
                .addClass(Post.class)
                .addClass(JpaEntity.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void testReplacement() throws Exception {
        Post post = new Post();
        post.setText("Pane redaktore vole, víte, co je to pussy?");

        post = (Post) processor.processItem(post);
        Assert.assertEquals("Pane redaktore <CENZUROVÁNO>, víte, co je to <CENZUROVÁNO>?", post.getText());
    }

}
