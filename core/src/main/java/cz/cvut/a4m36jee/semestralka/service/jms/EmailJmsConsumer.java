package cz.cvut.a4m36jee.semestralka.service.jms;

import cz.cvut.a4m36jee.semestralka.model.dto.EmailDto;
import cz.cvut.a4m36jee.semestralka.service.util.email.EmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

/**
 * Sends emails. Listens to JMS messages and when one is received, sends it.
 *
 * @author Tomas Milata
 * @since 24.1.15.
 */
@MessageDriven(name = "EmailSender", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = QueueNames.EMAIL_QUEUE),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class EmailJmsConsumer implements MessageListener {

    @Inject
    private EmailSender sender;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Invoked when JMS message is received.
     */
    @Override
    public void onMessage(Message message) {

        if (message instanceof ObjectMessage) {
            ObjectMessage msg = (ObjectMessage) message;
            try {
                Serializable serializable = msg.getObject();
                if (serializable instanceof EmailDto) {
                    EmailDto email = (EmailDto) serializable;
                    sender.sendEmail(email);
                } else {
                    log.error("Wrong serializable type. Serialized object {}", serializable);
                }
            } catch (JMSException e) {
                log.error("Could not get serialized message.", e);
            }
        } else {
            log.error("Wrong message type. Message {}", message);
        }
    }
}
