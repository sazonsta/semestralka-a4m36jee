package cz.cvut.a4m36jee.semestralka.model.dto;

import java.util.Date;

/**
 * @author Tomas Milata
 */
public class TopicDto extends AbstractDto {

    private Integer id;
    private String name;
    private Date createdOn;

    private Integer createdById;

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Integer createdById) {
        this.createdById = createdById;
    }
}