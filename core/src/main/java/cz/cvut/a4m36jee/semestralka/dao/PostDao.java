package cz.cvut.a4m36jee.semestralka.dao;

import cz.cvut.a4m36jee.semestralka.model.entity.Post;

import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Tomas Milata
 * @since 24.1.15.
 */
@Stateless
public class PostDao extends AbstractDao<Post> {

    public List<Post> getAllPosts() {
        CriteriaQuery<Post> cq = em.getCriteriaBuilder().createQuery(Post.class);
        final Root<Post> postRoot = cq.from(Post.class);
        cq.select(postRoot);
        cq.orderBy(em.getCriteriaBuilder().asc(postRoot.<Date>get("date")));
        List<Post> result = em.createQuery(cq).getResultList();
        return result;
    }

    public List<Post> getAllNotDirtyCheckedPosts() {
        CriteriaQuery<Post> cq = createCriteriaQuery();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        Root<Post> postRoot = cq.from(Post.class);
        cq.select(postRoot).where(cb.isNull(postRoot.get("checkedDirtyWordsDate")));
        return em.createQuery(cq).getResultList();
    }

    private void validatePosts(Collection<Post> posts) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        for (Post z : posts) {
            Set<ConstraintViolation<Post>> constraintViolations = validator.validate(z);
            if (constraintViolations.size() > 0) {
                throw new RuntimeException(constraintViolations.toString());
            }
        }
    }

    @Override
    protected Class<Post> getEntityClass() {
        return Post.class;
    }
}
