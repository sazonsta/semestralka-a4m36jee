package cz.cvut.a4m36jee.semestralka.dao;

import cz.cvut.a4m36jee.semestralka.model.entity.Topic;

import javax.ejb.Stateless;

/**
 * @author Tomas Milata
 * @since 24.1.15.
 */
@Stateless
public class TopicDao extends AbstractDao<Topic> {
    @Override
    protected Class<Topic> getEntityClass() {
        return Topic.class;
    }
}
