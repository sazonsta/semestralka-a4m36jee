package cz.cvut.a4m36jee.semestralka.dao;


import cz.cvut.a4m36jee.semestralka.model.entity.JpaEntity;
import cz.cvut.a4m36jee.semestralka.model.entity.Post;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractDao<E extends JpaEntity> {

    @PersistenceContext(unitName = "a4m36jee-db", type = PersistenceContextType.TRANSACTION)
    protected EntityManager em;

    public E getById(int id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<E> cq = createCriteriaQuery();
        Root<E> entity = cq.from(getEntityClass());
        cq.select(entity).where(cb.equal(entity.get(JpaEntity.ID_COLUMN_NAME), id));
        return em.createQuery(cq).getSingleResult();
    }

    public List<E> getAll() {
        CriteriaQuery<E> cq = em.getCriteriaBuilder().createQuery(getEntityClass());
        cq.select(cq.from(getEntityClass()));
        return em.createQuery(cq).getResultList();
    }

    /**
     * Saves a new entity.
     *
     * @param entity to be persisted.
     */
    public void persist(E entity) {
        entity.setId(null);
        em.persist(entity);
    }

    /**
     * For updates.
     */
    public void merge(E entity) {
        em.merge(entity);
    }

    public void delete(E entity) {
        em.remove(entity);
    }

    protected abstract Class<E> getEntityClass();

    protected CriteriaQuery<E> createCriteriaQuery() {
        return em.getCriteriaBuilder().createQuery(getEntityClass());
    }
}
