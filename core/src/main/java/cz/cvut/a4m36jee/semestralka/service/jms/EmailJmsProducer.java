package cz.cvut.a4m36jee.semestralka.service.jms;

import cz.cvut.a4m36jee.semestralka.model.dto.EmailDto;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;

/**
 * Encapsulates access to the JMS queue for sending emails.
 *
 * @author Tomas Milata
 * @since 24.1.15.
 */
@Singleton
@JMSDestinationDefinitions({
        @JMSDestinationDefinition(
                name = QueueNames.EMAIL_QUEUE,
                interfaceName = "javax.jms.Queue")
}
)
public class EmailJmsProducer {
    @Inject
    private JMSContext context;

    @Resource(mappedName = QueueNames.EMAIL_QUEUE)
    private Destination queue;

    /**
     * Sends the message to the JMS queue.
     *
     * @param message the email to be sent
     */
    public void sendMessage(EmailDto message) {
        context.createProducer().send(queue, message);
    }
}
