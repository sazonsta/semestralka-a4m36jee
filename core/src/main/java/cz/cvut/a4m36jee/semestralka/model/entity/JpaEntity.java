package cz.cvut.a4m36jee.semestralka.model.entity;

import java.io.Serializable;

/**
 * All subclasses should have and "id" Integer attribute because of the
 * {@link cz.cvut.a4m36jee.semestralka.dao.AbstractDao#getById(int)} method.
 *
 * @author Tomas Milata
 * @since 24.1.15.
 */
public interface JpaEntity extends Serializable {
    String ID_COLUMN_NAME = "id";

    /**
     * DAO must be able to set id to null so that Entitymanager does not think it is a detached entity.
     */
    public void setId(Integer id);
}
