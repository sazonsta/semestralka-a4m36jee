package cz.cvut.a4m36jee.semestralka.security;

/**
 * @author Tomas Milata
 * @since 23.1.15.
 */
public class Role {
    public static final String ADMIN = "admin";
    public static final String MODERATOR = "moderator";
    public static final String USER = "user";
}
