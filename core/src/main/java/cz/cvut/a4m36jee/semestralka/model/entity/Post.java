package cz.cvut.a4m36jee.semestralka.model.entity;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the zaznam database table.
 * 
 */
@Entity(name = "POST")
public class Post implements JpaEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private String title;

	@Lob
	private String text;

	//bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	@ManyToOne
	private Topic topic;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CHECKED_DIRTY_WORDS_DATE")
    private Date checkedDirtyWordsDate;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

    public Date getCheckedDirtyWordsDate() {
        return checkedDirtyWordsDate;
    }

    public void setCheckedDirtyWordsDate(Date checkedDirtyWordsDate) {
        this.checkedDirtyWordsDate = checkedDirtyWordsDate;
    }
}