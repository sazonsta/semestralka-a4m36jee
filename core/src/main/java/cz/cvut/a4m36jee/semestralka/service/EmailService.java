package cz.cvut.a4m36jee.semestralka.service;

import cz.cvut.a4m36jee.semestralka.model.dto.EmailDto;
import cz.cvut.a4m36jee.semestralka.service.jms.EmailJmsProducer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

/**
 * @author Tomas Milata
 * @since 24.1.15.
 */
@Stateless
public class EmailService {

    @Inject
    private EmailJmsProducer jmsProducer;

    @Transactional
    public void sendEmail(EmailDto email) {
        jmsProducer.sendMessage(email);
    }
}
