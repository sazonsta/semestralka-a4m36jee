package cz.cvut.a4m36jee.semestralka.batch;

import cz.cvut.a4m36jee.semestralka.dao.PostDao;
import cz.cvut.a4m36jee.semestralka.model.entity.Post;

import javax.batch.api.chunk.AbstractItemWriter;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Tomas Milata
 * @since 31.1.15.
 */
@Named
public class DirtyWordsBatchWriter extends AbstractItemWriter {

    @Inject
    private PostDao postDao;

    @Override
    public void writeItems(List<Object> items) throws Exception {
        items.forEach(p -> postDao.merge((Post) p));
    }
}
