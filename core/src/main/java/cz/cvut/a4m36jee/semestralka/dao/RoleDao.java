package cz.cvut.a4m36jee.semestralka.dao;

import cz.cvut.a4m36jee.semestralka.model.entity.Role;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * @author Tomas Milata
 * @since 2.2.15.
 */
public class RoleDao extends AbstractDao<Role> {

    @Override
    protected Class<Role> getEntityClass() {
        return Role.class;
    }

    public Role getByName(String name) {
        CriteriaQuery<Role> cq = createCriteriaQuery();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        Root<Role> roleRoot = cq.from(getEntityClass());
        cq.select(roleRoot).where(cb.equal(roleRoot.get("name"), name));
        return em.createQuery(cq).getSingleResult();
    }
}
