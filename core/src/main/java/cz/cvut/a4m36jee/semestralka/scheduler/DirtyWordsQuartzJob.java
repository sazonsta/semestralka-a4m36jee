package cz.cvut.a4m36jee.semestralka.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import java.util.Properties;

/**
 * @author Tomas Milata
 * @since 1.2.15.
 */
public class DirtyWordsQuartzJob implements Job {

    private final Logger log = LoggerFactory.getLogger(DirtyWordsQuartzJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        long id = jobOperator.start("dirty-words", new Properties());
        log.info("Running dirty words batch job with id {}.", id);
    }
}
