package cz.cvut.a4m36jee.semestralka.scheduler;


import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;


/**
 * @author Tomas Milata
 * @since 1.2.15.
 */
@Startup
@Singleton
public class DirtyWordsScheduler {

    private static final String DIRTY_WORDS_GROUP = "dirtyWordsGroup";

    private final Logger log = LoggerFactory.getLogger(DirtyWordsScheduler.class);

    private Scheduler scheduler;

    @PostConstruct
    public void scheduleJobs() {

        JobDetail job = JobBuilder.newJob(DirtyWordsQuartzJob.class)
                .withIdentity("dirtyWordsJob", DIRTY_WORDS_GROUP).build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("myTrigger", DIRTY_WORDS_GROUP)
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(10)
                        .repeatForever())
                .build();

        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            if (!scheduler.checkExists(job.getKey())) {
                scheduler.scheduleJob(job, trigger);
            }
            scheduler.start();
        } catch (SchedulerException e) {
            log.error("Error while creating scheduler", e);
        }
    }


    @PreDestroy
    public void stopJobs() {
        if (scheduler != null) {
            try {
                scheduler.shutdown(false);
            } catch (SchedulerException e) {
                log.error("Error while closing scheduler", e);
            }
        }
    }
}