package cz.cvut.a4m36jee.semestralka.batch;

import cz.cvut.a4m36jee.semestralka.dao.PostDao;
import cz.cvut.a4m36jee.semestralka.model.entity.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.api.chunk.AbstractItemReader;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Tomas Milata
 * @since 31.1.15.
 */
@Named
public class DirtyWordsBatchReader extends AbstractItemReader {

    private final Logger log = LoggerFactory.getLogger(DirtyWordsBatchReader.class);

    @Inject
    private PostDao postDao;

    private ListIterator<Post> iterator;

    @Override
    public void open(Serializable checkpoint) throws Exception {
        List<Post> posts = postDao.getAllNotDirtyCheckedPosts();
        iterator = posts.listIterator();
        log.info("Starting dirty words batch job with {} posts.", posts.size());
    }

    @Override
    public Object readItem() throws Exception {
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }
}
