package cz.cvut.a4m36jee.semestralka.service;

import cz.cvut.a4m36jee.semestralka.dao.RoleDao;
import cz.cvut.a4m36jee.semestralka.dao.UserDao;
import cz.cvut.a4m36jee.semestralka.model.dto.EmailDto;
import cz.cvut.a4m36jee.semestralka.model.dto.UserDto;
import cz.cvut.a4m36jee.semestralka.model.entity.User;
import cz.cvut.a4m36jee.semestralka.security.Role;
import org.apache.commons.codec.digest.DigestUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * @author Tomas Milata
 * @since 15.1.15.
 */
@Stateless
public class UserService extends AbstractService {

    @Inject
    private UserDao userDao;

    @Inject
    private EmailService emailService;

    @Inject
    private RoleDao roleDao;

    @Transactional
    public List<UserDto> getAllUsers() {
        return mapList(userDao.getAllUsers(), UserDto.class);
    }

    @Transactional
    public void addUser(UserDto userDto) {
        User user = objectMapper.map(userDto, User.class);
        user.setPassword(DigestUtils.sha256Hex(userDto.getPassword()));
        user.setRoles(Collections.singleton(roleDao.getByName(Role.USER)));
        userDao.persist(user);

        notifyNewUser(user);
    }

    private void notifyNewUser(User user) {
        final EmailDto email = new EmailDto();

        email.setFrom("noreply@forum.fm");
        email.setTo(user.getEmail());
        email.setSubject("Registration Completed.");
        email.setBody("You were registered to to forum.");

        emailService.sendEmail(email);

    }

    @Transactional
    public UserDto getByUsername(String name) {
        return objectMapper.map(userDao.getByUsername(name), UserDto.class);
    }
}
