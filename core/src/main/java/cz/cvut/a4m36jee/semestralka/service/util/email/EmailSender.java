package cz.cvut.a4m36jee.semestralka.service.util.email;

import cz.cvut.a4m36jee.semestralka.model.dto.EmailDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Tomas Milata
 * @since 24.1.15.
 */
@ApplicationScoped
public class EmailSender {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public void sendEmail(EmailDto email) {
        log.info("Sent email: \n{}", email);
    }
}
