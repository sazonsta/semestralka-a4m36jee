package cz.cvut.a4m36jee.semestralka.batch;

import cz.cvut.a4m36jee.semestralka.model.entity.Post;

import javax.batch.api.chunk.ItemProcessor;
import javax.inject.Named;
import java.util.Date;

/**
 * @author Tomas Milata
 * @since 31.1.15.
 */
@Named
public class DirtyWordsBatchProcessor implements ItemProcessor {

    private static final String DIRTY_REGEX = "vole|pussy";

    @Override
    public Object processItem(Object item) throws Exception {
        Post p = (Post) item;
        p.setText(removeDirty(p));
        p.setCheckedDirtyWordsDate(new Date());
        return p;
    }

    private String removeDirty(Post p) {
        return p.getText().replaceAll(DIRTY_REGEX, "<CENZUROVÁNO>");
    }
}
