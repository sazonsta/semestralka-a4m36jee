package cz.cvut.a4m36jee.semestralka.model.entity;

import javax.persistence.*;

/**
 * @author Tomas Milata
 * @since 2.2.15.
 */
@Entity(name = "ROLE")
public class Role implements JpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Override
    public void setId(Integer id) {

    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
