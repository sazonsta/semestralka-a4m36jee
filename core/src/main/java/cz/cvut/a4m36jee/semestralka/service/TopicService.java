package cz.cvut.a4m36jee.semestralka.service;

import cz.cvut.a4m36jee.semestralka.dao.PostDao;
import cz.cvut.a4m36jee.semestralka.dao.TopicDao;
import cz.cvut.a4m36jee.semestralka.dao.UserDao;
import cz.cvut.a4m36jee.semestralka.model.dto.TopicDto;
import cz.cvut.a4m36jee.semestralka.model.entity.Topic;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Tomas Milata
 * @since 1.2.15.
 */
@Stateless
public class TopicService extends AbstractService {

    @Inject
    private TopicDao topicDao;

    @Inject
    private PostDao postDao;

    @Inject
    private UserDao userDao;

    @Transactional
    public List<TopicDto> getAllTopics() {
        return mapList(topicDao.getAll(), TopicDto.class);
    }

    @Transactional
    public void addTopic(TopicDto topicDto) {
        Topic topic = objectMapper.map(topicDto, Topic.class);
        topic.setCreatedBy(userDao.getById(topicDto.getCreatedById()));
        topicDao.persist(topic);
    }

    @Transactional
    public void deleteTopic(TopicDto topicDto) {
        Topic topic = topicDao.getById(topicDto.getId());
        topic.getPosts().forEach(postDao::delete);
        topicDao.delete(topic);
    }

    @Transactional
    public TopicDto getById(TopicDto topicDto) {
        return objectMapper.map(topicDao.getById(topicDto.getId()), TopicDto.class);
    }
}
