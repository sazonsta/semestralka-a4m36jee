package cz.cvut.a4m36jee.semestralka.service.jms;

/**
 * @author Tomas Milata
 * @since 25.1.15.
 */
public class QueueNames {
    public static final String EMAIL_QUEUE = "java:global/jms/emailQueue";
}
