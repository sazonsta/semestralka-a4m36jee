package cz.cvut.a4m36jee.semestralka.service;

import cz.cvut.a4m36jee.semestralka.dao.PostDao;
import cz.cvut.a4m36jee.semestralka.dao.TopicDao;
import cz.cvut.a4m36jee.semestralka.dao.UserDao;
import cz.cvut.a4m36jee.semestralka.model.dto.PostDto;
import cz.cvut.a4m36jee.semestralka.model.dto.TopicDto;
import cz.cvut.a4m36jee.semestralka.model.entity.Post;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Tomas Milata
 * @since 15.1.15.
 */
@Stateless
public class PostService extends AbstractService {

    @Inject
    private PostDao postDao;

    @Inject
    private UserDao userDao;

    @Inject
    private TopicDao topicDao;

    @Transactional
    public List<PostDto> getAllPosts() {
        return mapList(postDao.getAllPosts(), PostDto.class);
    }

    @Transactional
    public void addPost(PostDto postDto) {
        Post post = objectMapper.map(postDto, Post.class); // maps the primitive values

        post.setUser(userDao.getById(postDto.getUserId())); // set referenced entities by hand
        post.setTopic(topicDao.getById(postDto.getTopicId()));

        postDao.persist(post);
    }

    @Transactional
    public void removePost(PostDto postDto) {
        Post post = postDao.getById(postDto.getId());
        postDao.delete(post);
    }

    public List<PostDto> getPostsForTopic(TopicDto topicDto) {
        return mapList(topicDao.getById(topicDto.getId()).getPosts(), PostDto.class);
    }
}