package cz.cvut.a4m36jee.semestralka.service;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides common functions of business layer (service) classes.
 *
 * @author Tomas Milata
 * @since 15.1.15.
 */
public abstract class AbstractService {

    /**
     * For mapping JPA entities to DTOs and vice versa.
     */
    protected Mapper objectMapper = DozerBeanMapperSingletonWrapper.getInstance();

    /**
     * Translates a list to a list of objects of the target class.
     */
    protected <T> List<T> mapList(Collection<?> source, Class<T> targetClass) {
        List<T> result = new ArrayList<>(source.size());
        for (Object o : source) {
            result.add(objectMapper.map(o, targetClass));
        }
        return result;
    }
}
