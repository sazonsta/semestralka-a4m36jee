package cz.cvut.a4m36jee.semestralka.model.dto;

import java.util.Set;

/**
 * @author Tomas Milata
 * @since 15.1.15.
 */
public class UserDto extends AbstractDto {

    private Integer id;
    private String email;
    private String name;
    private String surname;
    private String username;
    private String password;

    private Set<String> rolesNames;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRolesNames() {
        return rolesNames;
    }

    public void setRolesNames(Set<String> rolesNames) {
        this.rolesNames = rolesNames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
