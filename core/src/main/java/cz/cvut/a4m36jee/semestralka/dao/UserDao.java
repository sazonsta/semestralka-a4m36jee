package cz.cvut.a4m36jee.semestralka.dao;

import cz.cvut.a4m36jee.semestralka.model.entity.User;

import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Tomas Milata
 * @since 24.1.15.
 */
@Stateless
public class UserDao extends AbstractDao<User> {

    public List<User> getAllUsers() {
        CriteriaQuery<User> cq = em.getCriteriaBuilder().createQuery(User.class);
        cq.select(cq.from(User.class));
        List<User> result = em.createQuery(cq).getResultList();
        return result;
    }

    private void validateUsers(Collection<User> users) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        for (User u : users) {
            Set<ConstraintViolation<User>> constraintViolations = validator.validate(u);
            if (constraintViolations.size() > 0) {
                throw new RuntimeException(constraintViolations.toString());
            }
        }
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    public User getByUsername(String name) {
        CriteriaQuery<User> cq = em.getCriteriaBuilder().createQuery(User.class);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        final Root<User> userRoot = cq.from(User.class);
        cq.select(userRoot).where(cb.equal(userRoot.get("username"), name));
        return em.createQuery(cq).getSingleResult();

    }
}
