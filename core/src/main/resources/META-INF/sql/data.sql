INSERT INTO ROLE(ID, NAME) VALUES (100001, 'admin');
INSERT INTO ROLE(ID, NAME) VALUES (100002, 'moderator');
INSERT INTO ROLE(ID, NAME) VALUES (100003, 'user');

INSERT INTO USER(ID, NAME, SURNAME, USERNAME, PASSWORD, EMAIL) VALUES (1, 'Petr', 'Novák', 'pepa', '321cb76b7d6e43ef6917ee54fd8fa0e7fa99ba97c7e0b6fc7866139e7cb8c9c3', 'p-novak@abcd.cz');
INSERT INTO USER(ID, NAME, SURNAME, USERNAME, PASSWORD, EMAIL) VALUES (2, 'Jan', 'Novotný', 'j-novotny', 'aaa', 'j-novotny@abcd.cz');
INSERT INTO USER(ID, NAME, SURNAME, USERNAME, PASSWORD, EMAIL) VALUES (3, 'Ondra', 'Macháček', 'o-machacek', 'aaa','o-machacek@abcd.cz');
INSERT INTO USER(ID, NAME, SURNAME, USERNAME, PASSWORD, EMAIL) VALUES (4, 'Big', 'God', 'god', '5723360ef11043a879520412e9ad897e0ebcb99cc820ec363bfecc9d751a1a99', 'god@seznam.abc');
INSERT INTO USER(ID, NAME, SURNAME, USERNAME, PASSWORD, EMAIL) VALUES (5, 'Leoš', 'Mareš', 'leos',  'f03688c424f453b4598166e1ce52fcfa77e30512556d6a7787b17c56c19e5357', 'leos@mares.xxx');

INSERT INTO TOPIC(ID, CREATED_BY_USER_ID, CREATED_ON, NAME) VALUES (111, 1, '2013-12-30 10:15:03', 'Okurková sezóna');
INSERT INTO TOPIC(ID, CREATED_BY_USER_ID, CREATED_ON, NAME) VALUES (112, 1, '2013-12-30 10:15:03', 'Fotbal');
INSERT INTO TOPIC(ID, CREATED_BY_USER_ID, CREATED_ON, NAME) VALUES (113, 1, '2013-12-30 10:15:03', 'Miloš Zeman');
INSERT INTO TOPIC(ID, CREATED_BY_USER_ID, CREATED_ON, NAME) VALUES (114, 4, '2013-12-30 10:15:03', 'Konspirační Teorie');
INSERT INTO TOPIC(ID, CREATED_BY_USER_ID, CREATED_ON, NAME) VALUES (115, 5, '2013-12-30 10:15:03', 'Ornella Štiková');

INSERT INTO POST(USER_ID, TITLE, TEXT, DATE, TOPIC_ID) VALUES (2, 'Na Šumavě ráno naměřili minus 27,2 stupně', 'Meteorologové naměřili v sobotu ráno na některých místech teploty pod minus 20 stupňů Celsia. Nejchladněji bylo na Šumavě - v Rokytské...', '2014-12-30 10:15:03', 111);
INSERT INTO POST(USER_ID, TITLE, TEXT, DATE, TOPIC_ID) VALUES (1, 'Otužilci v Praze překonali rekord', 'Teploty kolem nuly přivítaly v pátek téměř 290 odvážlivců na 68. ročníku vánočního Memoriálu Alfreda Nikodéma - nejstarší otužilecké...', '2014-12-30 10:17:32', 111);
INSERT INTO POST(USER_ID, TITLE, TEXT, DATE, TOPIC_ID) VALUES (3, 'V pěti krajích Česka hrozí náledí', 'Aktualizováno - V Karlovarském, Plzeňském, Ústeckém, Libereckém a Jihočeském kraji hrozí tvorba náledí. Český hydrometeorologický ústav varuje, že...', '2014-12-30 11:25:13', 111);
INSERT INTO POST(USER_ID, TITLE, TEXT, DATE, TOPIC_ID) VALUES (1, 'Víkend bude ledový, teploty už se letos nad nulu nepodívají', 'Druhý svátek vánoční je letos pravděpodobně posledním dnem, kdy se teploty na některých místech České republiky ještě udrží lehce...', '2014-12-30 11:45:57', 111);

INSERT INTO USER2ROLE(USER_ID, ROLE_ID) VALUES (1, 100003);
INSERT INTO USER2ROLE(USER_ID, ROLE_ID) VALUES (2, 100003);
INSERT INTO USER2ROLE(USER_ID, ROLE_ID) VALUES (3, 100003);

INSERT INTO USER2ROLE(USER_ID, ROLE_ID) VALUES (4, 100001);

INSERT INTO USER2ROLE(USER_ID, ROLE_ID) VALUES (5, 100002);