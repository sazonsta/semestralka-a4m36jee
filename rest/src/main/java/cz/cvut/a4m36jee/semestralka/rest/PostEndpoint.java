package cz.cvut.a4m36jee.semestralka.rest;

import cz.cvut.a4m36jee.semestralka.model.dto.PostDto;
import cz.cvut.a4m36jee.semestralka.model.dto.UserDto;
import cz.cvut.a4m36jee.semestralka.security.Role;
import cz.cvut.a4m36jee.semestralka.service.PostService;
import cz.cvut.a4m36jee.semestralka.service.UserService;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author Tomas Milata
 * @since 15.1.15.
 */
@Path("post")
@DenyAll
public class PostEndpoint {

    @Inject
    private PostService postService;

    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({Role.ADMIN, Role.MODERATOR, Role.USER})
    public List<PostDto> getAllUsers() {
        return postService.getAllPosts();
    }
}
