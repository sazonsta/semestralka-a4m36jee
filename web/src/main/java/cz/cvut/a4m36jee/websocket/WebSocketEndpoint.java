package cz.cvut.a4m36jee.websocket;

/**
 * @author Tomas Milata
 * @since 2.2.15.
 */
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint("/clients")
public class WebSocketEndpoint {
    static Set<Session> clients = Collections.newSetFromMap(new ConcurrentHashMap<>());


    /**
     * Method receive incoming web socket messages and sends back number of all connected clients.
     *
     * @param text   receiving text but (without usage)
     * @param client Session of client (without usage)
     */
    @OnMessage
    public void onMessage(String text, Session client) {
        client.getAsyncRemote().sendText(Integer.toString(clients.size()));
    }

    /**
     * Method is called when new websocket session is open, add new session into set of clients session and sends updated count of clients to all clients
     *
     * @param client new websocket session
     */
    @OnOpen
    public void onOpen(Session client) {
        boolean added = clients.add(client);
        if (added) {
            String size = Integer.toString(clients.size());
            for (Session cl : clients) {
                cl.getAsyncRemote().sendText(size);
            }
        }
    }

    /**
     * Method is called when websocket session is closed, removes from set of clients session and sends updated count of clients to all clients
     *
     * @param client closed websocket session
     */
    @OnClose
    public void onClose(Session client) {
        boolean removed = clients.remove(client);
        if (removed) {
            String size = Integer.toString(clients.size());
            for (Session cl : clients) {
                if (cl.isOpen()) {
                    cl.getAsyncRemote().sendText(size);
                }
            }
        }
    }

    /**
     * Method handles error on session, removes session from set and sends updated count of clients to all clients
     *
     * @param client session where error was occurred
     * @param t      Throwable exception
     */
    @OnError
    public void onError(Session client, Throwable t) {
        boolean removed = clients.remove(client);
        if (removed) {
            String size = Integer.toString(clients.size());
            for (Session cl : clients) {
                if (cl.isOpen()) {
                    cl.getAsyncRemote().sendText(size);
                }
            }
        }
    }
}