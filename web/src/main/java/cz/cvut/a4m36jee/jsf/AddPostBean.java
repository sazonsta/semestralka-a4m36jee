package cz.cvut.a4m36jee.jsf;


import cz.cvut.a4m36jee.semestralka.model.dto.PostDto;
import cz.cvut.a4m36jee.semestralka.service.PostService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Stanislav Sazonov
 * @since 18.1.15.
 */
@ManagedBean
@RequestScoped
public class AddPostBean {

    @Inject
    private CurrentUserBean currentUserBean;

    @Inject
    private PostService postService;

    @Size(min = 1, message = "Title is not set")
    private String title;

    @Size(min = 1, message = "Text is not set")
    private String text;

    public String addPost(int topicId) {
        PostDto post = new PostDto();
        post.setDate(new Date());
        post.setTitle(title);
        post.setText(text);
        post.setUserId(currentUserBean.getUserDto().getId());
        post.setTopicId(topicId);

        postService.addPost(post);
        return "posts?topicId=" + topicId + "faces-redirect=true";

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}