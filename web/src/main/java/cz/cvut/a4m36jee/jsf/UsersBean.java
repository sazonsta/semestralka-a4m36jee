package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.UserDto;
import cz.cvut.a4m36jee.semestralka.service.UserService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Tomas Milata
 * @since 15.1.15.
 */
@ManagedBean
@RequestScoped
@Named("users")
public class UsersBean {

    @Inject
    private UserService userService;

    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }
}
