package cz.cvut.a4m36jee.jsf;


import cz.cvut.a4m36jee.semestralka.model.dto.TopicDto;
import cz.cvut.a4m36jee.semestralka.service.TopicService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Stanislav Sazonov
 * @since 18.1.15.
 */
@ManagedBean
@RequestScoped
public class AddTopicBean {

    @Inject
    private TopicService topicService;

    @Inject
    private CurrentUserBean user;

    @Size(min=1, message = "Name is not set.")
    private String name;

    public String createTopic(){
        TopicDto dto = new TopicDto();
        dto.setName(name);
        dto.setCreatedOn(new Date());
        dto.setCreatedById(user.getUserDto().getId());
        topicService.addTopic(dto);
        return "topics?faces-redirect=true";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}