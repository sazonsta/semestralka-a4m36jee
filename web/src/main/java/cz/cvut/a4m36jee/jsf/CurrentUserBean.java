package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.UserDto;
import cz.cvut.a4m36jee.semestralka.security.Role;
import cz.cvut.a4m36jee.semestralka.service.UserService;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.security.Principal;

/**
 * @author Tomas Milata
 * @since 1.2.15.
 */
@ManagedBean
@SessionScoped
@Named("currentUser")
public class CurrentUserBean {

    private UserDto userDto;

    @EJB
    private UserService userService;

    public UserDto getUserDto() {
        if (userDto == null) {
            Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principal != null) {
                userDto = userService.getByUsername(principal.getName());
            }
        }
        return userDto;
    }

    public boolean isAdmin() {
        return getUserDto().getRolesNames().contains(Role.ADMIN);
    }

    public boolean isModerator() {
        return getUserDto().getRolesNames().contains(Role.MODERATOR);
    }

    public boolean isUser() {
        return getUserDto().getRolesNames().contains(Role.USER);
    }

}