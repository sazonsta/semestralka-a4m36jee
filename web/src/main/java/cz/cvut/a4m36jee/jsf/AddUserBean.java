package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.UserDto;
import cz.cvut.a4m36jee.semestralka.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJBTransactionRolledbackException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Size;

@ManagedBean
@RequestScoped
public class AddUserBean {

    private final Logger log = LoggerFactory.getLogger(AddUserBean.class);

    @Inject
    private UserService userService;

    @Size(min = 1, message = "Name is not set")
    private String name;

    @Size(min = 1, message = "Surname is not set")
    private String surname;

    @Size(min = 1, message = "Username is not set")
    private String username;

    @Size(min=5, message = "Email is not set correctly")
    private String email;

    @Size(min = 2, message = "Too short.")
    private String password;

    private UIComponent usernameInputField;

    public String createUser(){
        UserDto dto = new UserDto();
        dto.setName(name);
        dto.setSurname(surname);
        dto.setUsername(username);
        dto.setEmail(email);
        dto.setPassword(password);
        try {
            userService.addUser(dto);
        }
        catch(EJBTransactionRolledbackException ex){
            showDuplicateUsernameMessage();
        }
        return "/topics?faces-redirect=true";
    }

    private void showDuplicateUsernameMessage(){
        FacesMessage message = new FacesMessage("Username already exists.");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(usernameInputField.getClientId(context), message);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UIComponent getUsernameInputField() {
        return usernameInputField;
    }

    public void setUsernameInputField(UIComponent usernameInputField) {
        this.usernameInputField = usernameInputField;
    }
}
