package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.PostDto;
import cz.cvut.a4m36jee.semestralka.model.dto.TopicDto;
import cz.cvut.a4m36jee.semestralka.service.PostService;
import cz.cvut.a4m36jee.semestralka.service.TopicService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * @author Tomas Milata
 * @since 15.1.15.
 */
@ManagedBean(name = "postsBean")
@SessionScoped
public class PostsBean {

    @Inject
    private PostService postService;

    @Inject
    private TopicService topicService;

    private String topicId;

    public Integer getTopicId() {
        if (topicId == null) {
            Map<String, String> params =
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            topicId = params.get("topicId");
        }
        return Integer.valueOf(topicId);
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public List<PostDto> getPosts() {

        final TopicDto topicDto = new TopicDto();
        topicDto.setId(getTopicId());
        return postService.getPostsForTopic(topicDto);
    }

    public TopicDto getTopic() {
        final TopicDto topicDto = new TopicDto();
        topicDto.setId(getTopicId());
        return topicService.getById(topicDto);
    }
}
