package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.PostDto;
import cz.cvut.a4m36jee.semestralka.service.PostService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

@ManagedBean(name= "deletePost")
@RequestScoped
public class DeletePostBean {

    @Inject
    private PostService postService;

    public String delete(int postId, int topicId) {
        final PostDto postDto = new PostDto();
        postDto.setId(postId);
        postService.removePost(postDto);
        return "posts?topicId=" + topicId + "faces-redirect=true";
    }

}