package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.TopicDto;
import cz.cvut.a4m36jee.semestralka.service.TopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Stanislav Sazonov
 * @since 1.2.15.
 */
@ManagedBean
@RequestScoped
@Named("topics")
public class TopicsBean {

    private Logger log = LoggerFactory.getLogger(TopicsBean.class);

    @Inject
    private TopicService topicService;

    public List<TopicDto> getAllTopics() {
        List<TopicDto> topics = topicService.getAllTopics();
        log.info("topics size: {}", topics.size());
        return topics;
    }
}
