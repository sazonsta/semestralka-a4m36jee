package cz.cvut.a4m36jee.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * @author Tomas Milata
 * @since 24.1.15.
 */
@ManagedBean
@RequestScoped
@Named("logout")
public class LogoutBean {

    public String logout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.invalidateSession();
        return "/topics?faces-redirect=true";
    }
}
