package cz.cvut.a4m36jee.jsf;

import cz.cvut.a4m36jee.semestralka.model.dto.TopicDto;
import cz.cvut.a4m36jee.semestralka.service.TopicService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

@ManagedBean(name= "deleteTopic")
@RequestScoped
public class DeleteTopicBean {

    @Inject
    private TopicService topicService;

    public String delete(int id) {
        final TopicDto topicDto = new TopicDto();
        topicDto.setId(id);
        topicService.deleteTopic(topicDto);
        return "topics?faces-redirect=true";
    }
	
}